
import { StackNavigator } from 'react-navigation';
import App from '../app'
import CinemaDashboard from './Cinema-admin/CinemaDashboard'


const NavigationApp = StackNavigator({
  Home: { screen: App },
  CinemaDashboard: { screen: CinemaDashboard },
});