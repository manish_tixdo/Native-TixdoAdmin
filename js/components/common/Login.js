import React, { Component } from 'react'
import { connect } from 'react-redux'
import { login } from '../../actions/authActions'
import { Text,View,TextInput,Button } from 'react-native';


class Login extends Component {

  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      userInfo: {},

    }

//    this._handleUsernameChange = this._handleUsernameChange.bind(this)
//    this._handlePasswordChange = this._handlePasswordChange.bind(this)
    this.handelLogin = this.handelLogin.bind(this)

  }

//  _handleUsernameChange (value) {
//
//    this.setState({
//      username: value
//    })
//  }
//
//  _handlePasswordChange (value) {
//
//    this.setState({
//      password: value
//    })
//  }
//
////
  checkCredentials(){
    if(this.state.username ===''||this.state.password ===''){
      return false
    }
    else{
      return true
    }
  }


  handelLogin () {
    let cred = this.checkCredentials();
    if(cred === true) {
      let credentials = {
        email: this.state.username,
        password: this.state.password
      }
      this.props.authenticate(credentials)
//      if (e.keyCode === 13) {
//        let credentials = {
//          email: this.state.username,
//          password: this.state.password
//        }
//        this.props.authenticate(credentials)
//      }
    }
    else{
//      this.openToastrhost('no cred')
    }

  }

  componentWillReceiveProps (newProps) {
      console.log(newProps,"newprops")
      this.props.navigation('CinemaDashboard')

// if (typeof(newProps.user.key) !== 'undefined') {
//
//     this.props.navigation('CinemaDashboard')
//    }
//    else {
////      this.openToastrhost(newProps.statusText)
//    }

  }



  componentDidMount () {

//    if ((this.props.user !== null) && (typeof this.props.user.key !== 'undefined' && this.props.isServer && typeof this.props.user.group !== 'undefined' && this.props.user.group !== null)) {
//      this.setUser(this.props)
//    }
//    else {
//
//    }

  }

  componentWillUnmount () {

  }

  render () {
  console.log(this.props.user,"user")

    return (
        <View>
            <TextInput
                placeholder='Username'
                style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                onChangeText={(username) => this.setState({username})}
                value={this.state.username}
              />
            <TextInput
                placeholder='Password'
                secureTextEntry
                style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                onChangeText={(password) => this.setState({password})}
                 alue={this.state.password}
            />
            <Button
                  onPress={() => this.handelLogin()}
                  title="Login"
                  color="#357ce2"
                  accessibilityLabel="Please Login"
            />

        </View>

    )
  }
}

const mapStateToProps = (state) => {

  return {
    user: state.auth.user,
    statusCode: state.auth.statusCode,
    statusText: state.auth.statusText,
  }
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    authenticate: (user) => {
      dispatch(login(user))

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
