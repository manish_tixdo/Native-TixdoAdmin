/**
 * Created by consultadd on 17/2/17.
 */
import { combineReducers } from 'redux'
import authReducer from './authReducer'


export default combineReducers({
  auth: authReducer,

})