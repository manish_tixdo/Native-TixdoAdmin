import { URLS } from '../components/common/url-constants'
import {get_promise, post_promise } from './restActions'
import { types } from './types'

export function login (user) {
  return dispatch => {
    let dataObj = {
      input_value: user.email,
      password: user.password
    }

    return post_promise(URLS.DASHBOARDLOGIN, dataObj).then(response => {

      dispatch({
        type: types.save_user,
        payload: response
      })
    }, error => {
      dispatch({
        type: types.invalid_user,
        payload: {statusText: error.response.data.error_desc || error.response}
      })
    })

  }
}
export function logout () {
  return dispatch => {

    return post_promise(URLS.AUTH_USER_LOGOUT, {}).then(response => {
      Router.push('/')
      dispatch({
        type: types.remove_user,
      })

    })
  }
}



